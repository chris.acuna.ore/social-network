const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt'); 
const auth = require('../../middleware/auth');
const jwt = require('jsonwebtoken'); // J W T
const config = require('config'); // C O N F I G
const { check, validationResult } = require('express-validator'); // V A L I D A T I O N

const User = require('../../models/User');

// @route     GET api/auth
// @desc      Test route
// @access    Public

router.get('/', auth, async (req, res) => {
  try{
    const user = await User.findById(req.user.id).select('-password');
    res.json(user);
  }catch(err){
    console.log(err.message);
    res.status(500).send('Server Error');
  }
});

// @route     POST api/auth
// @desc      Authenticate user & get token
// @access    Public

router.post('/', [
  check('email', 'Please include a valid email').isEmail(),
  check('password', 'Password is required').exists()
], async (req, res) =>{
  const errors = validationResult(req);
  if(!errors.isEmpty()){
      return res.status(400).json({ errors: errors.array() });
  }

  const { email, password } = req.body;

  try{
    let user = await User.findOne({ email });

    if(!user){
      return res.status(400).json({ errors: [{ msg: 'Invalid credentials' }] });
    }   
    
    const isMatch = await bcrypt.compare(password, user.password);

    const payload = {
      user: {
        id: user.id
      }
    }

    if(!isMatch){
      return res.status(400).json({ errors: [{ msg: 'Invalid credentials' }] });
    }

    jwt.sign(payload, 
            config.get('jwtSecret'), 
            {expiresIn: 360000}, 
            (err, token)=>{
              if(err) throw err;
              res.json({ token });
            }
    );

  }catch(err){
    res.status(500).send('Server error');
  }
})



module.exports = router;